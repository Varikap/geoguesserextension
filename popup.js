const timerLimitInput = document.getElementById('timerLimit');
const selectElement = document.getElementById('audio');

timerLimitInput.addEventListener('change', function () {
  const timerLimit = parseInt(timerLimitInput.value);
  if (!isNaN(timerLimit)) {
    chrome.storage.sync.set({ timerLimit }, function () {
      chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        const activeTab = tabs[0];
        chrome.scripting.executeScript({
          target: { tabId: activeTab.id },
          function: null,
          args: [timerLimit]
        });
      });
    });
  }
});

selectElement.addEventListener('change', function () {
  const audio = selectElement.value;
  const audioElement = new Audio(chrome.runtime.getURL(`sounds/${audio}`))
  audioElement.play()
  chrome.storage.sync.set({ audio }, function () {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      const activeTab = tabs[0];
      chrome.scripting.executeScript({
        target: { tabId: activeTab.id },
        function: null,
        args: [audio]
      });
    });
  });
})

chrome.storage.sync.get(['timerLimit', 'audio'], function (result) {
  timerLimitInput.value = result.timerLimit || 0;
  selectElement.value = result.audio || 'click1.mp3';
});
