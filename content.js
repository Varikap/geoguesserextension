let timerLimit = 5

function checkTimerAndPlaySound() {
  const targetTimerElement = document.querySelector('[class^="styles_timer-value__"]')

  if (targetTimerElement) {
    const timerText = targetTimerElement.textContent
    chrome.storage.sync.get(['timerLimit', 'audio'], function (result) {
      timerLimit = result.timerLimit || 0;
      const [minutes, seconds] = timerText.split(':')
      const totalSeconds = parseInt(minutes) * 60 + parseInt(seconds)

      if (!isNaN(totalSeconds) && totalSeconds <= timerLimit && totalSeconds > 0) {
        const audio = new Audio(chrome.runtime.getURL(`sounds/${result.audio || 'click1.mp3'}`))
        audio.play()
      }
    });
  }
}

setInterval(() => checkTimerAndPlaySound(), 1000)